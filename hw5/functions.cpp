//Nick Scandura
//601.220
//11-1-17, hw6
//nscandu1, nscandu1@jhu.edu

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include "functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::ifstream;
using std::vector;
using std::swap;


void load(map<string, int> &data);
void edits(string st, vector<string> &sug);
void insertions(string st, vector <string> &sug);
void deletions(string st, vector <string> &sug);
void transpositions(string st, vector <string> &sug);
void substitutions(string st, vector <string> &sug);
void known(vector<string> sug, map<string, int> data, map<int, string> &correcs);
string correct(const string input, map<string, int> data);



void load(map<string, int> &data) {

  string file_name = "data/gutenberg.txt";
  string temp = "";
  ifstream inFile;
  inFile.open(file_name, std::ios::in);
  
  if (inFile.is_open()) {
    while (getline(inFile, temp)){
      
      std::stringstream str(temp);
      string current = "";
      int freq = 0;
      
      str >> current;
      str >> freq;

      data[current] = freq;
    }
    inFile.close();
  }
  else {
    cout << "There was a problem with opening the database" << endl;
  }  
  
}





void edits(const string st, vector<string> &sug) {

  insertions(st, sug);
  deletions(st, sug);
  transpositions(st, sug);
  substitutions(st, sug);

}



void insertions(const string st, vector <string> &sug) {

  string alph = "abcdefghijklmnopqrstuvwxyz";
  
  for (int i = 0; i < 26; i++) {
    for (unsigned int j = 0; j < st.length() + 1; j++) {
      string tester = st;
      string letter = alph.substr(i, 1);
      
      tester.insert(j, letter);

      sug.push_back(tester);
      
    }
  }
}



void deletions(const string st, vector <string> &sug) {

    for (unsigned int j = 0; j < st.length(); j++) {
      string tester = st;

      tester.erase(j, 1);

      sug.push_back(tester);
      
    }  
}


void transpositions(const string st, vector <string> &sug) {
  
  for (unsigned int j = 0; j < st.length() - 1; j++) {
    string tester = st;
    
    swap(tester[j], tester[j+1]);
    
    sug.push_back(tester);
  }
}


void substitutions(const string st, vector <string> &sug) {

  string alph = "abcdefghijklmnopqrstuvwxyz";
  
  for (int i = 0; i < 26; i++) {
    for (unsigned int j = 0; j < st.length(); j++) {
      string tester = st;
      string letter = alph.substr(i, 1);
      
      tester.replace(j, 1, letter);
      
      sug.push_back(tester);
    }
  }
}


void known(const vector<string> sug, map<string, int> data, map<int, string> &correcs) {

  
  map<string, int>::iterator itera;

  for(unsigned int x = 0; x < sug.size(); x++) {
    itera = data.find(sug[x]);
    if (itera != data.end()) {
      correcs[itera->second] = (itera->first);
    }
  
  }

}


string correct(const string input, map<string, int> data) {

  map<string, int>::iterator it;
  it = data.find(input);
  if (it != data.end()) {
    return input;
  }
  
  vector<string> suggestions;  
  edits(input, suggestions);

  map<int, string> correcs;
  known(suggestions, data, correcs);

  if(!correcs.empty()) {
    return correcs.rbegin()->second;
  }

  vector<string> secondary;
  
  for(vector<string>::iterator ite = suggestions.begin();
      ite != suggestions.end();
      ite++) {
    edits(*ite, secondary);
  }

  known(secondary, data, correcs);

  if(!correcs.empty()) {
    return correcs.rbegin()->second;
  }
    
  return "";
}
