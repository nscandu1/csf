#Nick Scandura HW5 nscandu1 nscandu1@jhu.edu
	
.data

	newline:  .asciiz "\n"
	space:	.asciiz " "	
.text
	
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	lw $t0, 4($a1)
	b read_input


read_input:	
	
	lb $t1, ($t0)
	move $a0, $t1
	beq $a0, 0, return
	subu $a0, $a0, 48
	bge $a0, 49, letter
	
        addi $t0, $t0, 1
	
	move $s1, $a0
	la $s7, 4
        b convert


letter:	
	subu $a0, $a0, 39

	addi $t0, $t0, 1
	
	move $s1, $a0
	la $s7, 4
        b convert
	
convert:
	
	beq $s7, 0, read_input
	and $s2, $s1, 1

	addu $s0, $s0, $s2
	srl $s1, $s1, 1
	subu $s7, $s7, 1
	b convert

return:
	move $a0, $s0
	
        #print
        li $v0, 1
        syscall
        li      $v0, 4
        la      $a0, newline
        syscall
	
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	li $v0, 10
	syscall
