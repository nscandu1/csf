# Nick Scandura HW5 nscandu1 nscandu1@jhu.edu CSF

1. Since Mips doesn't have an easy, automatic carry flag, I
   substituted my own carry flag. When a sum is computed, 
   I then compare it to the two operands and see if it is
   less than either operand. If the sum is less, I add 1 to the 
   next addition. For subtraction, I see if the result is greater
   than the operands and then subtract 1 from the next operation. I
   did this because I figured out that the sum would only be
   greater if there was a carry.
   
2. My code is in c++, so it can be built with a c++ compiler,
   I used the one in the ugrad system

3. My bubble sort takes the first element in the array
   and bubbles it forward by comparing it to the element
   directly in front of it. If the first element is greater, 
   they swap. Bubbling through the array repeats for the 
   size of the array

4. My population count first converts the command line 
   string into a hex number by extracting bytes from the
   command line argument and subtracting the correct 
   amount from it. Then, the hex is sent to a conversion 
   function, which shifts through each bit of the hex number
   and masks it to a register. The register is then added to
   a sum, which counts up all the set bits in that hex. The
   program then loops through again and grabs the next
   hex from the command line, sends it to the conversion
   function, and keeps adding to the sum until there is no
   more input from the command line.