//Nick Scandura HW5 nscandu1 nscandu1@jhu.edu CSF

#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::stoi;
using std::ifstream;
string unhex(unsigned long input);
unsigned long rotateLeft(unsigned long num);


int main () {
  unsigned long input = 0;
  cout << unhex(input) << endl;
  input = -1;
  cout << unhex(input) << endl;
  input = 65535;
  cout << unhex(input) << endl;
  input = 65536;
  cout << unhex(input) << endl;
}

string unhex(unsigned long input) {

  string digits = "0123456789abcdef";
  string storage = "";
  
  for (int i = 0; i < 8; i++) {
    input = rotateLeft(input);
    int copy = 0;
    copy = input & 15;
    char temp = digits.at(copy);
    storage = storage + temp;
  }


  return storage;

}


unsigned long rotateLeft(unsigned long num) {

  //take highest 4 bits
  unsigned long m = 4026531840;
  unsigned long mask = num & m;
  //remove from original number
  num = num & 268435455;
  //shift original
  num = num << 4;
  mask = mask >> 28;
  //add back in highest 4 bits
  num = num | mask;
  return num;
  
}
