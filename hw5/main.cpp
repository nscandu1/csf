//Nick Scandura
//601.220
//11-1-17, hw6
//nscandu1, nscandu1@jhu.edu

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include "functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::ifstream;
using std::vector;
using std::swap;


int main() {

  map<string, int> data;
  
  load(data);

  bool run = true;

  while(run) {
  
    string input = "";
  
    cin >> input;

    if(input == "quit") {
      break;
    }
    
    string output = "";
    
    output = correct(input, data);
    
    if(output.length() == 0) {
      cout << "No correction found." << endl;
    }
    else {
      cout << "Did you mean " << output << "?" << endl;
    }

  }
  
}
