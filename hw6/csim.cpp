//Nick Scandura, Nick Lemanski, CSF, HW6

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <map>
#include <sstream>
#include <math.h>
#include <vector>

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::string;
using std::stoi;
using std::ifstream;
using std::map;
using std::vector;
using std::stringstream;
int unhex(string input);

struct block {
  int blocktag;
  int blockoffset;
  int frequency;
  int timestamp;
  int dirty;
};


int main (int argc, char *argv[]) {

  //check if command line is top notch
  if (argc != 8) {
    cout << "Improper amount of command line arguments" << endl;
    exit(0);
  }
  
  //cache data
  int numsets = atoi(argv[1]);
  int numblocks = atoi(argv[2]);
  int numbytes = atoi(argv[3]);
  int wrtalc = atoi(argv[4]);
  int wrtthr = atoi(argv[5]);
  int lru = atoi(argv[6]);
  string filename = argv[7];
  
  //ERROR HANDLING
  //positive power of 2?
  if (((numsets & (numsets - 1)) != 0) && (numsets > 0)) {
    cout << "Number of sets is not a positive power of 2" << endl;
    exit(0);
  }
  if (((numblocks & (numblocks - 1)) != 0) && (numblocks > 0)) {
    cout << "Number of blocks is not a positive power of 2" << endl;
    exit(0);
  }
  if (((numbytes & (numbytes - 1)) != 0) && (numbytes > 3)) {
    cout << "Number of bytes is not a positive power of 2, or less than 4" << endl;
    exit(0);
  }
  //END OF ERROR HANDLING
  
  //tracking data
  //load or store to cache = 1 cycle
  //l or s to memory = 100 cycles x amt of 4 byte transfers
  int cycles = 0;
  int time = 0;

  //output variables
  int totalLoads = 0;
  int totalStores = 0;
  int loadHits = 0;
  int loadMisses = 0;
  int storeHits = 0;
  int storeMisses = 0;
  
  //set up the cache
  vector<int> sets;
  map<int, vector<block>> cache;

  if(wrtalc == 0 && wrtthr == 0){
    cerr << "You can't do no-write alloc and write-back" << endl;
    exit(0);
  }


  //file read
  ifstream inFile;

  //ERROR HANDLING
  inFile.open(filename);

  if (!inFile) {
    cout << "File does not exist" << endl;
    exit(0);
  }
  if (inFile.peek() == EOF) {
    cout << "File is empty" << endl;
    exit(0);
  }
  //END OF ERROR HANDLING

  
  while (inFile.peek() != EOF) {

    string line; getline(inFile, line);
    int address;
    int tag;
    int index;
    int offset;
    
    //save
    if (line.at(0) == 's') {

      totalStores++;

      //isolate address
      stringstream ss;
      ss << line;
      string adrs; ss >> adrs;
      adrs = ""; ss >> adrs;
      //ERROR HANDLING
      if (adrs.length() < 3) {
	cout << "Improper input" << endl;
	exit(0);
      }
      //END OF ERROR HANDLING
      adrs = adrs.substr(2);

      //Turn address to binary
      //ERROR HANDLING
      for (int a = 0; a < adrs.length(); a++) {
	if (!(isxdigit(adrs.at(a)))) {
	  cout << "Input is not hexadecimal!" << endl;
	  exit(0);
	}
      }
      //END ERROR HANDLING
      
      address = unhex(adrs);
      int temp = address;
      int mask = 0;

      //offset loop
      for (int i = 0; i < (log2(numbytes)); i++) {
	mask = mask << 1; mask = mask | 1;
      }
      offset = address & mask;
      for (int j = 0; j < (log2(numbytes)); j++) {
	address = address >> 1;
      }
      mask = 0;

      //index loop
      for (int a = 0; a < (log2(numsets)); a++) {
        mask = mask << 1; mask = mask | 1;
      }
      index = address & mask;
      for (int b = 0; b < (log2(numsets)); b++) {
        address = address >> 1;
      }

      //setting tag
      mask = 0;
      tag = address | mask; address = temp;

      //create a new block
      //block newblock; newblock.blocktag = tag; newblock.blockoffset = offset; newblock.timestamp = time;
      //newblock.dirty = 0; newblock.frequency = time;
      block newblock = {tag, offset, time, time, 0};

      //if it was a hit
      int hit = 0;
      for(map<int, vector<block>>::iterator it = cache.begin(); it!= cache.end(); ++it) {
        if(it->first == index){
          for(vector<block>::iterator iv = it->second.begin(); iv != cache[index].end(); ++iv){
	    if((*iv).blocktag == tag){
	      hit = 1;
	    }
          }
        }
      }

      //hit or miss actions
      if(hit == 1){
        storeHits++; //could replace but this is just a simulation so unneccssary just changes the value
        if(wrtthr == 1){
          cycles = cycles + 100*(numbytes/4); //update the value in memory
        } else {
          for(vector<block>::iterator it = cache[index].begin(); it != cache[index].end(); ++it){
            if((*it).blocktag == tag) {
	      (*it).dirty = 1;
              (*it).frequency = time;
            }
          }
        }
        cycles++;//update the value in the cache

      } else {
        storeMisses++;
        if(wrtalc == 1) { // pull from memory, treat as cache hit

          if(wrtthr == 1){
            cycles = cycles + 100*(numbytes/4); //update the value in memory
          } else {
	    newblock.dirty = 1;
          }

          cycles = cycles + 100*(numbytes/4);
          cache[index].push_back(newblock); //block from memory

          cycles++;//update the value in the cache

        } else { // wrtalc == 0 means only update memory, no cache
          cycles = cycles + 100*(numbytes/4);
        }

      }

      // Eviction process
      block *r;
      int min = (*cache[index].begin()).frequency;
      r = &(*cache[index].begin());
      int count = 0;
      if(cache[index].size() > numblocks) {
          for(vector<block>::iterator a = cache[index].begin(); a != cache[index].end(); ++a){
            if(lru == 0 && (*a).timestamp < min){
              min = (*a).timestamp;
              r = &(*a);
            }
            if(lru == 1 && (*a).frequency < min){
              min = (*a).frequency;
              r = &(*a);
            }
          }

          for(auto a : cache[index]){
            if(a.blocktag == (*r).blocktag){
              break;
            }
            count++;
          }

          cache[index].erase(cache[index].begin() + count);

          if((*r).dirty == 1){
            cycles = cycles + 100*(numbytes/4);
          }
      }   

      time++;
     
    }
    //load
    else if (line.at(0) == 'l') {

      totalLoads++;

      //isolate address
      stringstream ss;
      ss << line;
      string adrs; ss >> adrs; adrs = "";
      ss >> adrs;
      adrs = adrs.substr(2);

      //unhex address
      address = unhex(adrs);
      int temp = address;
      int mask = 0;

      //offset loop
      for (int i = 0; i < (log2(numbytes)); i++) {
        mask = mask << 1; mask = mask | 1;
      }
      offset = address & mask;
      for (int j = 0; j < (log2(numbytes)); j++) {
        address = address >> 1;
      }

      //index loop
      mask = 0;
      for (int a = 0; a < (log2(numsets)); a++) {
        mask = mask << 1; mask = mask | 1;
      }
      index = address & mask;
      for (int b = 0; b < (log2(numsets)); b++) {
        address = address >> 1;
      }

      //setting tag
      mask = 0;
      tag = address | mask; address = temp;
      
      //pretend block in memory
      //block newblock; newblock.blocktag = tag; newblock.blockoffset = offset; newblock.timestamp = time;
      //newblock.dirty = 0; newblock.frequency = time;
      block newblock = {tag, offset, time, time, 0};

      //if it was a hit
      int hit = 0;
      for(map<int, vector<block>>::iterator it = cache.begin(); it!= cache.end(); ++it) {
        if(it->first == index){
          for(vector<block>::iterator iv = it->second.begin(); iv != cache[index].end(); ++iv){
	    if((*iv).blocktag == tag){
	      hit = 1;
	    }
          }
        }
      }

      //hit or miss
      if(hit == 1){
        loadHits++;

        for(vector<block>::iterator it = cache[index].begin(); it != cache[index].end(); ++it){
          if((*it).blocktag == tag) {
              (*it).frequency = time;
          }
        }

      } else {
        loadMisses++;
        cache[index].push_back(newblock);//this is the block from memory
        cycles = cycles + 100*(numbytes/4);
      }
      cycles++;//update the value in the cache

      // Eviction process
      block *r;
      int min = (*cache[index].begin()).frequency;
      r = &(*cache[index].begin());
      int count = 0;
      if(cache[index].size() > numblocks) {
          for(vector<block>::iterator a = cache[index].begin(); a != cache[index].end(); ++a){
            if(lru == 0 && (*a).timestamp < min){
              min = (*a).timestamp;
              r = &(*a);
            }
            if(lru == 1 && (*a).frequency < min){
              min = (*a).frequency;
              r = &(*a);
            }
          }

          for(auto a : cache[index]){
            if(a.blocktag == (*r).blocktag){
              break;
            }
            count++;
          }

          cache[index].erase(cache[index].begin() + count);

          //If the memory address for r wasn't updated in memory it must be during eviction
          if((*r).dirty == 1){
            cycles = cycles + 100*(numbytes/4);
          }
      }

      time++;

    }
    else {
      cout << "Error: Input is neither a save nor a load." << endl;
      exit(0);
    }
  }

  
//  for(map<int, vector<block>>::iterator it = cache.begin(); it!= cache.end(); ++it) {
//    cout << it->second.size() << endl;
//    for(auto b : it->second) {
//      cout << b.blocktag << " " << it->first << " " << b.blockoffset << " " << b.timestamp << endl;
//    }
//  }

  //Print out final output variables 
  cout << "Total loads: " <<  totalLoads << endl;
  cout << "Total stores: " <<  totalStores << endl;
  cout << "Load hits: " <<  loadHits << endl;
  cout << "Load misses: " <<  loadMisses << endl;
  cout << "Store hits: " <<  storeHits << endl;
  cout << "Store misses: " <<  storeMisses << endl;
  cout << "Total cycles: " <<  cycles << endl;

}


int unhex(string input) {

  int output = 0;
  
  for (unsigned int x = 0; x < input.length(); x++) {
    
    char next = input.at(x);
    output = output << 4;
    
    switch(next) {
    case '0': output = output | 0;break;
    case '1': output = output | 1; break;
    case '2': output = output | 2; break;
    case '3': output = output | 3; break;
    case '4': output = output | 4; break;
    case '5': output = output | 5; break;
    case '6': output = output | 6; break;
    case '7': output = output | 7; break;
    case '8': output = output | 8; break;
    case '9': output = output | 9; break;
    case 'a': output = output | 10; break;
    case 'b': output = output | 11; break;
    case 'c': output = output | 12; break;
    case 'd': output = output | 13; break;
    case 'e': output = output | 14; break;
    case 'f': output = output | 15; break; 
    }
  }
    return output;
}
