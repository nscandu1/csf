	.globl	main		
main:
	leal	4(%esp), %ecx 	
	pushl	%ebp 		
	movl	%esp, %ebp 	
.L3:
	subl    $12, %esp	
	pushl	-12(%ebp) 	
	call	putchar 	
	addl    $16, %esp
.L2:
	call	getchar		
	movl	%eax, -12(%ebp) 
	cmpl	$-1, -12(%ebp)	
	jne	.L3
	leave
