	.comm buffer, 512
	.text

	.globl  _start
_start:	
	leal	4(%esp), %ecx 	
	pushl	%ebp 		
	movl	%esp, %ebp 	
.L3:
	subl    $12, %esp	
	pushl	-12(%ebp) 	
	##########write
	mov     %eax, %edx
        mov     $4, %eax                # system call 4 is write
        mov     $1, %ebx                # file handle 1 is stdout
        mov     $buffer, %ecx           # address of string to output
        int     $0x80                   # invoke operating system code
	##########
	addl    $16, %esp
.L2:
	##########read
	mov     $3, %eax
        mov     $0, %ebx
        mov     $buffer, %ecx
        mov     $buffer, %edx
        int     $0x80
	##########
	cmpl    $0, %eax
        jne     .L3

	mov     $1, %eax                # system call 1 is exit
        xor     %ebx, %ebx              # we want return code 0
        int     $0x80                   # invoke operating system code
