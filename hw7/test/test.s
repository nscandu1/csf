	.global _start
	.comm buffer, 131072
        .text
_start:

loop:
	# read(1, buffer, $1024)
	
	mov	$3, %eax
	mov	$0, %ebx
	mov	$buffer, %ecx
	mov	$buffer, %edx
	int	$0x80
	
        # write(1, message, 13)
	mov     %eax, %edx
	mov     $4, %eax                # system call 4 is write
        mov     $1, %ebx                # file handle 1 is stdout
        mov     $buffer, %ecx           # address of string to output
        int     $0x80                   # invoke operating system code

	cmpl	$-1, %eax
	je	exit
	
	cmpl	$0, %eax
	jne	loop
	

exit:	
        mov     $1, %eax                # system call 1 is exit
        xor     %ebx, %ebx              # we want return code 0
        int     $0x80                   # invoke operating system code
