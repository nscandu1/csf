	.globl	main
	
main:
.LFB5:

	pushq	%rbp	   #pushes the base pointer onto the stack
	movq	%rsp, %rbp	#moves the value of the stack pointer to the base pointer
	subq	$16, %rsp	#moves the stack pointer down 16 bytes, reserving 16 bytes for the program
	jmp	.L2		#jumps to local label 2 
.L3:
	movl	-4(%rbp), %eax	#puts the base pointer value in the %eax register
	movl	%eax, %edi	#puts the %eax register in the %edi address register
	call	putchar		#calls putchar, which takes the value from %edi and outputs it to stdout
.L2:
	call	getchar		#calls getchar, which grabs a character from stdin. This call is pretty directly copied from the original c program
	movl	%eax, -4(%rbp)	#register %eax contains the char from getchar, transfers it to the DEREFERNCED ADDRESS STORED IN THE BASE POINTER
	cmpl	$-1, -4(%rbp)	#checks for the null pointer by comparing the input and -1, this is the equivalent of  ((c = getchar()) != EOF)
	jne	.L3		#if not null pointer, jumps to local label 3
	leave			#LEAVES THE PROGRAM
