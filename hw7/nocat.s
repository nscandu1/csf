#section .data
string:	
	.ascii "hell"
string_end:
	.equ len, string_end - string

#section .text
	#.globl _start
	.globl main
main:
#_start:	

#	leal    4(%esp), %ecx
 #       pushl   %ebp
  #      movl    %esp, %ebp

	movl	$4, %eax # write is system call 4
	movl	$1, %ebx # arg1: stdout is "file" 1
	movl	string, %ecx # arg2: hello world string
	movl	$4, %edx # arg3: length of string
	int $0x80
	leave


	
#	leave
	
	#movq $60, %rax # exit is system call 60
	#movq $0, %rdi # exit status
	#syscall
