
	.globl	main
main:
.LFB5:
	leal	4(%esp), %ecx #adds 4 to the stack pointer, stores it in %ecx
#	andl	$-16, %esp
#	pushl	-4(%ecx)
	pushl	%ebp
	movl	%esp, %ebp
#	pushl	%ecx
#	subl	$20, %esp
	jmp	.L2
.L3:
#	subl	$12, %esp
	pushl	-12(%ebp)
	call	putchar
	addl	$16, %esp
.L2:
	call	getchar
	movl	%eax, -12(%ebp)
	cmpl	$-1, -12(%ebp)	 #checks for the null pointer by comparing the input and -1, this is the equivalent of  ((c = getchar()) != EOF)
	jne	.L3  #if not null pointer, jumps to local label 3
#	movl	$0, %eax
#	movl	-4(%ebp), %ecx
	leave
#	leal	-4(%ecx), %esp
