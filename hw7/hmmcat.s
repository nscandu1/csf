//      .file   "cat.c"
	.text
        .globl  main
//      .type   main, @function
main:
.LFB5:
        //.cfi_startproc
        pushq   %rbp       #pushes the base pointer onto the stack
        //.cfi_def_cfa_offset 16
        //.cfi_offset 6, -16
        movq    %rsp, %rbp      #moves the value of the stack pointer to the base pointer
        //.cfi_def_cfa_register 6
        subq    $16, %rsp       #moves the stack pointer down 16 bytes, reserving 16 bytes for the program
        jmp     .L2             #jumps to local label 2
.L3:
        movl    -4(%rbp), %eax  #puts the base pointer value in the %eax register
        movl    %eax, %edi      #puts the %eax register in the %edi address register
        call    putchar         #calls putchar, which takes the value from %edi and outputs \
it to stdout
.L2:
        call    getchar         #calls getchar, which grabs a character from stdin. This cal\
l is pretty directly copied from the original c program
        movl    %eax, -4(%rbp)  #register %eax contains the char from getchar, transfers it \
to the DEREFERNCED ADDRESS STORED IN THE BASE POINTER
        cmpl    $-1, -4(%rbp)   //checks for the null pointer by comparing the input and -1, \
this is the equivalent of  ((c = getchar()) != EOF)
        jne     .L3             //if not null pointer, jumps to local label 3
        movl    $0, %eax        //empties the %eax register
        leave                   //LEAVES THE PROGRAM
        //.cfi_def_cfa 7, 8
        ret                     //RETURNS
        //.cfi_endproc
.LFE5:
//      .size   main, .-main
//      .ident  "GCC: (GNU) 7.2.1 20170915 (Red Hat 7.2.1-2)"
//      .section        .note.GNU-stack,"",@progbits
