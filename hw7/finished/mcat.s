#Nick Scandura, nscandu1@jhu.edu, nscandu1
#CSF Hw7
	
	.globl	main		
main:
	leal	4(%esp), %ecx 	#adds 4 to the stack pointer, stores it in %ecx
	pushl	%ebp 		#pushes the base pointer onto the stack
	movl	%esp, %ebp 	#moves the stack pointer value to the base pointer
.L3:
	subl    $12, %esp	#subtracts 12 from the stack pointer
	pushl	-12(%ebp) 	#pushes (base pointer - 12) onto the stack
	call	putchar 	#calls putchar, which takes the value from %edi and outputs it to stdout. This is directly from the c code
	addl    $16, %esp	#adds back 16 to the stack pointer to put it in the position it was before running this label
.L2:
	call	getchar		#calls getchar, which grabs a character from stdin. This call is directly from the c code
	movl	%eax, -12(%ebp) #register %eax contains the char from getchar, transfers it to (base pointer - 12)
	cmpl	$-1, -12(%ebp)	#checks for the null pointer by comparing the input and -1, this is the equivalent of  ((c = getchar()) != EOF)
	jne     .L3  		#if not null pointer, jumps to local label 3
	leave			#exits the program
