#Nick Scandura, nscandu1@jhu.edu, nscandu1
#CSF Hw7

	.global _start
	.comm buffer, 131072
        .text
_start:

loop:
	# read
	mov	$3, %eax
	mov	$0, %ebx
	mov	$buffer, %ecx
	mov	$buffer, %edx
	int	$0x80
	
        # write
	mov     %eax, %edx
	mov     $4, %eax
        mov     $1, %ebx
        mov     $buffer, %ecx
        int     $0x80

	cmpl	$-1, %eax
	je	exit
	
	cmpl	$0, %eax
	jne	loop
	

exit:	
        mov     $1, %eax
        xor     %ebx, %ebx
        int     $0x80
